<?php session_start(); ?> <!--Sessionlara ulaşmak için belirttik. -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="adminstyle.css">
</head>
<body>
<?php


if($_POST)
{
    //bu değişkenler formda yazı başlığı,yazı içeriği,tarih,aktifpasif kontrolü gibi değişkenlerin name 
    //ile buraya aktarılmasıdır.
	$ekle=$_POST["ekle"]; 
$ekle2=$_POST["ekle2"];
$ekle3=$_POST["ekle3"];
$ekle4=$_POST["aktifbutton"];
$guncellenecek_id=$_POST["gizliid"]; //burada görünmeyen bir input koyduk. duzenlenecek olan gönderinin id sini alabilmek için.
$db=new PDO('mysql:host=localhost;dbname=php_case','root',''); //db bağlantı cümlemiz kendi host umuz olduğu için localhost dedik root kullanıcı adı ve şifre yok
$query=$db->prepare("update yazilar set yazi_baslik=?,yazi=?,tarih=?,aktifpasif=? where id=?");
 $ekle=$query->execute(array($ekle,$ekle2,$ekle3,$ekle4,$guncellenecek_id));
 
header("location:admin_yazi_sil.php"); //işlem olduktan sonra yönlendirme yaptık.

}

//bu sefer post değil get metodu ile alıyoruz bilgileri. fetch fonksiyonu ile
//seçtiğimiz id li gönderiyi forma yazdırırız
if($_GET){

    $id=$_GET["id"];
    $db=new PDO('mysql:host=localhost;dbname=php_case','root','');
    $query=$db->query("select * from yazilar where id='{$id}'")->fetch(PDO::FETCH_ASSOC);
    if($query)
    {
        $yazi=$query["yazi"];
        $yazi_baslik=$query["yazi_baslik"];
        $tarih=$query["tarih"];
    }


}
?>



    <?php
    
    //Giriş yapılmayınca gösterilecek alanlar if kontrolü içerisinde belirtilir.
    if(!isset($_SESSION["ID"]))  //Eğer id isimli session yoksa yani giriş yapılmamışsa..
    {

 
echo '<a  style="color:purple; font-size:20px;" href="index.php">Lütfen Admin Girişi Yapmak İçin Tıklayınız</a>';

    }
//Giriş yapıldığı zaman gösterilecek alanlar else kontrolü içerisinde belirtilir.
    else {  
        echo  ' <div class="all">
        <div class="admin-header">
        <div class="header-text">
        <h3>Admin Paneli</h3>
        <div class="header-greet">
        <span>'.$_SESSION["kullanici_adi"].'</span>
        <a href="exit.php" class="logout-btn"><svg width="14" height="14" viewBox="0 0 24 24"><path fill="#fff" d="M14 12h-4v-12h4v12zm4.213-10.246l-1.213 1.599c2.984 1.732 5 4.955 5 8.647 0 5.514-4.486 10-10 10s-10-4.486-10-10c0-3.692 2.016-6.915 5-8.647l-1.213-1.599c-3.465 2.103-5.787 5.897-5.787 10.246 0 6.627 5.373 12 12 12s12-5.373 12-12c0-4.349-2.322-8.143-5.787-10.246z"/></svg></a>
        </div>
        </div>
        </div>
        <div class="admin-sidebar">
       
        <a href="admin_yazi_ekle.php">  <li>Ekle  </li></a>
       
         
        <a href="admin_yazi_sil.php">   <li>Sil-Düzenle  </li></a>
      
   
     
        <li>
        <a href="exit.php">Çıkış</a>
        </li>
        </div>
        <div class="center-content">
        <div class="all-border">
        <div class="shows-location">
        <div class="location-text">
        <span class="location">Admin >> Yazı Düzenle</span>
        </div>
        </div>
     
<form action="duzenle.php" method="POST">
<input type="hidden" name="gizliid" value="'.$id.'">
<input style="width:300px; padding:7px; border-radius:10px;" type="text" name="ekle" value="'.$yazi_baslik.'"> </br></br>
<textarea class="ckeditor"  name="ekle2">'.$yazi.'</textarea></br>

<input style="width:146px; padding:7px; border-radius:10px;" type="date" name="ekle3" value="'.$tarih.'"></br></br>
  <select style="width:166px; padding:7px; border-radius:10px;" name="aktifbutton" >
    <option value="1">Aktifleştir</option>
    <option value="0">Pasifleştir</option>
    
  </select> </br></br>
<input style="border-radius:10px;width:171px; font-weight:bold; color:white; background-color:#485b50; padding:10px;" type="submit" value="Düzenle">
</form> 
         </div>
        </div>
        </div>
';
    }

    //<textarea içerisinde ckeditor bilgisi ile önceden kütüphane olarak yukarda eklediğimiz ckeditor js eklentisini dahil ettik ve textarea mızı bir editöre çevirdik.
    ?>


 
<script src="//cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
        <script src="adminscript.js"></script>
</body>
</html>