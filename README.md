
---

## Proje Bilgileri


1. Girişte bizi **index.php** sayfası karşılıyor ana sayfa olarak.
2. Giriş butonu ile **admin_login.php** sayfasına geçiş yapılıyor ve kullanıcı admin girişi yapılıyor.
3. **admin_login.php** sayfasında kullanıcı ve parola kontrolü yapılarak admin.php yönetim paneline yönlendiriliyor.
4. Admin arayüzünde **Ekle** butonu ile **admin_yazi_ekle.php** sayfasına geçiş yapılıyor.
5. **admin_yazi_ekle.php** sayfasında yazı eklenebiliyor. Blog yazıları eklenirken **ckeditor metin editörü** kullanılmıştır.
6. Admin arayüzünde **Sil-Düzenle** butonu ile **admin_yazi_sil.php** sayfasına geçiş yapılıyor.
7. **admin_yazi_sil.php** sayfasında sil butonu ile **GET ile id** sini aldığımız bir blog yazısını silebiliyoruz. Ya da Düzenle butonu ile **duzenle.php** sayfasına geçebiliyoruz.
8. duzenle.php'de tüm blog yazısı bilgilerini değiştirebiliriz. Ya da blogu **Aktif/Pasif** hale getirip görünür/görünmez kılabiliriz.
9. Admin arayüzünde iki adet **çıkış butonu** ile **index.php'ye** yönlendirilir.


---

## Ekran Görüntüleri

---
![picture](https://i.hizliresim.com/BCi2TD.jpg)
---
![picture](https://i.hizliresim.com/6xGk4l.jpg)
---
![picture](https://i.hizliresim.com/AVdeEg.jpg)
---
![picture](https://i.hizliresim.com/piwIxm.jpg)
---
![picture](https://i.hizliresim.com/9J2glv.jpg)
---
![picture](https://i.hizliresim.com/EztbmC.jpg)
---
![picture](https://i.hizliresim.com/9Sm3ZZ.jpg)
---
![picture](https://i.hizliresim.com/00MukU.jpg)
---
![picture](https://i.hizliresim.com/mENBHD.jpg)
---