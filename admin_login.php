<?php include "login.php"; ?> <!--include ile login.php yi dahil ettik tüm login kodları orda mevcut-->
<!DOCTYPE html>
 
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ana Sayfa</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="style.css">
        <script src="script.js"></script>

        <style type="text/css">body{background-color: #7a3073;}</style>
    </head>
    <body>
 
        
    <div id="log-in">
  <form class="login-form" method="post">
    <h1 class="login-heading">Giriş Yap</h1>
     <div class="field-container -username">
      <input type="text" name="kullanici_adi" placeholder="Kullanıcı adını giriniz" />
    </div>
    <div class="field-container -password">
     
      <input type="password" name="parola" placeholder="Parolayı giriniz"/>
    </div>
    <button class="log-in-button" type="submit">Giriş</button>
    <a  href="index.php" style="text-decoration: none; color:#413cc4; text-align: center;">Ana Sayfaya Dön</a>
    </form>
</div>

     </body>
</html>