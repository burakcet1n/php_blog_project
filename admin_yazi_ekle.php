<?php session_start(); ?> <!--Sessionlara ulaşmak için belirttik. -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Php Case Project</title>
    <link rel="stylesheet" href="adminstyle.css">
</head>
<body>

    <?php
    

       $db=new PDO('mysql:host=localhost;dbname=php_case','root',''); //db bağlantı cümlemiz kendi host umuz olduğu için localhost dedik root kullanıcı adı ve şifre yok
       $gorevler=$db->query("select * from yazilar"); //bu kod ile db üzerinde yaptığımız bağlantı üzerinden tüm kayıtlar getirilir.



    ?>

    <?php
    
    //Giriş yapılmayınca gösterilecek alanlar if kontrolü içerisinde belirtilir.
    if(!isset($_SESSION["ID"]))  //Eğer id isimli session yoksa yani giriş yapılmamışsa..
    {

 
echo '<a  style="color:purple; font-size:20px;" href="index.php">Lütfen Admin Girişi Yapmak İçin Tıklayınız</a>';

    }
//Giriş yapıldığı zaman gösterilecek alanlar else kontrolü içerisinde belirtilir.
    else {  
        echo  ' <div class="all">
        <div class="admin-header">
        <div class="header-text">
        <h3>Admin Paneli</h3>
        <div class="header-greet">
        <span>'.$_SESSION["kullanici_adi"].'</span>
        <a href="exit.php" class="logout-btn"><svg width="14" height="14" viewBox="0 0 24 24"><path fill="#fff" d="M14 12h-4v-12h4v12zm4.213-10.246l-1.213 1.599c2.984 1.732 5 4.955 5 8.647 0 5.514-4.486 10-10 10s-10-4.486-10-10c0-3.692 2.016-6.915 5-8.647l-1.213-1.599c-3.465 2.103-5.787 5.897-5.787 10.246 0 6.627 5.373 12 12 12s12-5.373 12-12c0-4.349-2.322-8.143-5.787-10.246z"/></svg></a>
        </div>
        </div>
        </div>
        <div class="admin-sidebar">
       
        <a href="admin_yazi_ekle.php">  <li>Ekle  </li></a>
       
         
        <a href="admin_yazi_sil.php">   <li>Sil-Düzenle  </li></a>
      
 
       
     
       
        <a href="exit.php">  <li>Çıkış </li></a>
        
        </div>
        <div class="center-content">
        <div class="all-border">
        <div class="shows-location">
        <div class="location-text">
        <span class="location">Admin >> Yazı Ekle</span>
        </div>
        </div>
     
<form action="ekle.php" method="POST"> 
<input style="width:300px; padding:10px; border-radius:10px;" type="text" name="ekle" placeholder="Yazı başlığını girin"> </br></br>
<textarea class="ckeditor"  name="ekle2"></textarea></br>

<input  style="width:146px; padding:10px; border-radius:10px;" type="date" name="ekle3"></br></br>
<input style="border-radius:10px;width:171px; font-weight:bold; color:white; background-color:#485b50; padding:10px;" type="submit" value="Kaydet">
</form> 
         </div>
        </div>
        </div>
';
    }

    //<textarea içerisinde ckeditor bilgisi ile önceden kütüphane olarak yukarda eklediğimiz ckeditor js eklentisini dahil ettik ve textarea mızı bir editöre çevirdik.
    ?>


 
<script src="//cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
        <script src="adminscript.js"></script>
</body>
</html>