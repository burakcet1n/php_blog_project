<?php session_start(); ?> <!--Sessionlara ulaşmak için belirttik. -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Php Case Project</title>
    <link rel="stylesheet" href="adminstyle.css">
</head>
<body>

   <?php
    

       $db=new PDO('mysql:host=localhost;dbname=php_case','root','');
       $blog=$db->query("select * from yazilar Order By id DESC LIMIT 10"); //bu kod ile db üzerinde yaptığımız bağlantı üzerinden tüm kayıtlar getirilir. Ve son 10 kayıtı getirme kontrolünü LIMIT ile sağlarız



    ?>



    <?php
    
    //Giriş yapılmayınca gösterilecek alanlar if kontrolü içerisinde belirtilir.
    if(!isset($_SESSION["ID"]))  //Eğer id isimli session yoksa yani giriş yapılmamışsa..
    {

 //Eğer admin_login.php'den kullanıcı adı parola girişi yapılmazsa uyarı verir ve admin paneline ilerletmez.
echo '<a  style="color:purple; font-size:20px;" href="index.php">Lütfen Admin Girişi Yapmak İçin Tıklayınız</a>';

    }
//Giriş yapıldığı zaman gösterilecek alanlar else kontrolü içerisinde belirtilir.
    //kullanıcı adı bilgisini session ile aldık ve kim giriş yapıyorsa onu yazdırdık
    else {  
         setlocale(LC_TIME,'turkish');

        echo  ' <div class="all">
        <div class="admin-header">
        <div class="header-text">
        <h3>Admin Paneli</h3>
        <div class="header-greet">
        <span>'.$_SESSION["kullanici_adi"].'</span> 
        <a href="exit.php" class="logout-btn"><svg width="14" height="14" viewBox="0 0 24 24"><path fill="#fff" d="M14 12h-4v-12h4v12zm4.213-10.246l-1.213 1.599c2.984 1.732 5 4.955 5 8.647 0 5.514-4.486 10-10 10s-10-4.486-10-10c0-3.692 2.016-6.915 5-8.647l-1.213-1.599c-3.465 2.103-5.787 5.897-5.787 10.246 0 6.627 5.373 12 12 12s12-5.373 12-12c0-4.349-2.322-8.143-5.787-10.246z"/></svg></a>
        </div>
        </div>
        </div>
        <div class="admin-sidebar">
       
        <a href="admin_yazi_ekle.php">  <li>Ekle  </li></a>
       
       
       
        <a href="admin_yazi_sil.php"> <li>Sil-Düzenle  </li></a>
      
       
     
        
        <a href="exit.php"> <li>Çıkış  </li></a>
       
        </div>
        <div class="center-content">
        <div class="all-border">
        <div class="shows-location">
        <div class="location-text">
        <span class="location">Admin >> Ana Sayfa</span>
        </div>
        </div>
     
  
         </div>
        </div>
        </div>
';
    }
    ?>


 

        <script src="adminscript.js"></script>
</body>
</html>